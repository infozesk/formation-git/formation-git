# git-training

## À faire

- Retrouvez les slides de la présentation là:
  - [slides PDF](slides/adv_development.pdf) - clic-droit > sauver sous
- Installer git sur votre laptop OU créez un compte sur gitpod.io

### Installez git sur votre laptop

- Téléchargez et installez git pour:
  - windows: [git-scm](https://git-scm.com/download/win)
  - linux:
    - debian: `sudo apt get install git gitk`
    - centos: `sudo yum install git gitk`
- (optionel) Téléchargez et installez [code](https://code.visualstudio.com/Download)

### OU crééez un compte sur gitlab.com ET gitpod.io

## Licence

[Creative Commons CC BY 3.0](LICENCE.md)
